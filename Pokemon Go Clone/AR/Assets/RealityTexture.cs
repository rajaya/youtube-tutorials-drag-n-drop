﻿using UnityEngine;
using System.Collections;

public class RealityTexture : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Renderer renderer = GetComponent<Renderer>();

        WebCamTexture webCamTexture = new WebCamTexture(WebCamTexture.devices[0].name);

        renderer.material.mainTexture = webCamTexture;

        webCamTexture.Play();

        transform.localScale = new Vector3(1, (float)webCamTexture.height / (float)webCamTexture.width, 1);
	}
	
}
