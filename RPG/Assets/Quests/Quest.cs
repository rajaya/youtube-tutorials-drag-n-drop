﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Quest {
	public bool isComplete {get; private set;}
	public bool isFailed {get; private set;}

	public List<Objective> objectives;
	public void PassEvent (QuestEvent questEvent){
		bool tempIsComplete = true;
		bool tempIsFailed = false;
		foreach (Objective objective in objectives){
			objective.PassEvent (questEvent);
			if (!objective.isComplete) tempIsComplete = false;
			if (objective.isFailed) tempIsFailed = true;
		}
		Debug.Log (tempIsComplete);
		isComplete = tempIsComplete;
		isFailed = tempIsFailed;
	}
}

[System.Serializable]
public class Objective {
	public bool isComplete {
		get{
			if(isFailObjective) return true;
			return _isComplete;
		} 
		private set{
			_isComplete = value;
		}
	}
	public bool isFailed {get; private set;}
	private bool _isComplete;
	[SerializeField] string description;
	[SerializeField] string entityOfInterest;
	[SerializeField] QuestEventType questEventType;
	[SerializeField] int noOfEvents;
	[SerializeField] bool isFailObjective;

	int noOfEventsInternal;

	public void PassEvent (QuestEvent questEvent){
		if (questEventType != questEvent.questEventType) return;
		if (entityOfInterest != questEvent.entity) return;
		noOfEventsInternal++;
		if (noOfEvents >= noOfEventsInternal){
			if(isFailObjective){
				isFailed = true;
			} else {
				_isComplete = true;
			}
		}
	}
}