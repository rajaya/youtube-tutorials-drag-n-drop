using UnityEngine;
using System.Collections;

public abstract class Attack : MonoBehaviour
{
	public abstract void DoAttack (Vector2 target);
}

