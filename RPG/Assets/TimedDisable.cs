﻿using UnityEngine;
using System.Collections;

public class TimedDisable : MonoBehaviour {

	public float time;

	// Use this for initialization
	void OnEnable () {
		StartCoroutine (DisableMe());
	}

	IEnumerator DisableMe(){
		yield return new WaitForSeconds (time);
		gameObject.SetActive (false);
	}
}
